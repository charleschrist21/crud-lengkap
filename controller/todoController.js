const Todos = require('../model/todoModels')

exports.test = function(req,res){
    res.send("controler dan routes terhubung")
};

exports.create = function(req, res){
    let todos = new Todos({
        name: req.body.name,
        done: false
    });
    todos.save(function(err){
        if(err) throw err;
        res.send("success created");
    })
}

exports.todoShow = function(req,res){
    Todos.find({}, function(err,todos){
        if(err)throw err;
        res.send(todos);
    })
}
exports.todoDetail = function(req, res){
    Todos.findById(req.params.id, function(err, todos){
        if(err) throw err;
        res.send(todos);
    } )
}
exports.todoUpdate = function(req, res){
    Todos.findByIdAndUpdate(req.params.id, {$set : req.body}, function(err,todos){
        if(err) throw err;
        res.send("berhasil diupdate");
    })

}
exports.todoDelete = function(req, res){
    Todos.findByIdAndRemove(req.params.id, function(err){
        if(err) throw err;
        res.send("data dihapus");
    })
}