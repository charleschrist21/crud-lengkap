const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const todos = require("./routes/todoRoutes");
const mongoose = require("mongoose");

let mongourl = "mongodb://localhost:27017/todoapi";

const mongoDB = mongourl;
mongoose.connect(mongoDB);
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error cobak cek lagi"));


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended : false}));

app.use("/api", todos);

app.listen(3000,()=>{
    console.log('localhost run in port 3000')
})