const express = require('express');
const router = express.Router();

const todoController =require("../controller/todoController");

router.get("/test", todoController.test);
router.post("/create", todoController.create);
router.get("/", todoController.todoShow);
router.get("/:id", todoController.todoDetail);
router.put("/update/:id", todoController.todoUpdate);
router.delete("/:id/delete", todoController.todoDelete);

module.exports = router;